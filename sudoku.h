#pragma once
#include <QObject>
#include <QVector>
#include <QString>
#include <QList>
#include <possible.h>

struct Step
{
    int x, y, digit;
    const bool isValid;
    Step() : isValid(false) {}
    Step(int x, int y, int digit) : x(x), y(y), digit(digit), isValid(true){}
    bool operator==(const Step& right)
    {
        return this->x == right.x && 
                this->y == right.y &&
                this->digit == right.digit;
    }
};

enum LoggedType{ SIMPLE, GUESS, FALLBACK, NO_LOG };

struct LoggedRecord{
    int x, y, digit;
    LoggedType type;
    LoggedRecord(int x, int y, int digit, LoggedType type = SIMPLE):
        x(x), y(y), digit(digit), type(type){}
};

class Prediction{
public:
    int x;
    int y;
    int value;
    QVector <QVector<int>> backupMatrix;
    Possible backupPossible;
    Prediction();
    Prediction(int x, int y, int value, QVector <QVector<int>> matrix, Possible possible);
};

class Sudoku : public QObject
{
    Q_OBJECT
    int current_step_index = 0;
    QVector <QVector<int>> matrix;
    QString initial_string;
    Possible possible;
    QVector<Prediction> guesses;
    QList<LoggedRecord> loggedSteps;
    void makeGuess();
    void correctGuess();
    QVector<int> getPossible(int x, int y) const;
    bool isSolved() const;
    void saveGuess(int x, int y, int digit);
    void setDigit(int x, int y, int digit, LoggedType stepType = SIMPLE);
    Prediction fallbackToNGuesses(int n);
    Prediction fallbackToGuess(int x, int y);
    int matrixElement(int x, int y) const;
    Step findGuessPoint();
    void cleanUpForUserPlay();
public:
    Q_INVOKABLE void clear(bool clear_logged_steps = true);
    Q_INVOKABLE QVariantMap userStep(int index, int digit);
    Sudoku(QObject *parent = nullptr);
    Q_INVOKABLE bool setInitialString(QString input_string);
    Q_INVOKABLE QString getString();
    void solve();
    Q_INVOKABLE void prepare();
    Q_INVOKABLE QString makeItteration();
    Q_INVOKABLE void setDefaultOddEven(QList<int> default_odd_even);
    Q_INVOKABLE bool solveAndShow();
signals:
    void updateVisualState();
};


