import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 360
    height: 616
    title: qsTr("Sudoku")
    Item {
        id: name
        anchors.fill: parent
//        MouseArea{
//            anchors.fill: parent
//            onClicked: {focus = true; console.log("should be defocused")}
//        }
        
        Matrix{
            id: matrix
            anchors.fill: parent
        }

        Text{
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            textFormat: Text.RichText;
            text: "author: <style>a:link { color: #5f6368 ; }</style>" +
                  "<a href=" + link + ">Kiops</a>";
//            text: "author: Kiops"
            color: "#5f6368"
            property url link: "https://vk.com/kiops"
            onLinkActivated: Qt.openUrlExternally(link)
            anchors.margins: height*0.5
        }
        
        //        Row{
        //            anchors.horizontalCenter: parent.horizontalCenter
        //            anchors.top: matrix.bottom
        //            Button{
        //                text: "Set initial"
        //                onClicked: matrix.setState();
        //            }
        //            Button{
        //                text: "Make itteration"
        //                onClicked: matrix.makeItteration();
        //            }
        //            Button{
        //                text: "Clear"
        //                onClicked: matrix.clear();
        //            }
        //        }
    }
}
