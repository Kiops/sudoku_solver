import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import Sudoku 1.0
import QtGraphicalEffects 1.0
import QtQuick.Window 2.12

Item {
    id: mainItem
    
    Popup {
        id: popup
        width: popup_text.width*1.5
        height: popup_text.height*1.5
        anchors.centerIn: parent
        modal: true
        focus: true
        Text{
            id: popup_text
            text: "Wrong sudoku input"
            font.pointSize: 27
            anchors.centerIn: parent
        }
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    }
    
    
    Sudoku{
        id: sudoku
        onUpdateVisualState: {
            matrix.setCells(sudoku.getString());
        }
    }
    Column{
        anchors.centerIn: parent
        spacing: bottom_row.height*0.1
        width: matrix.width
    GridView{
        id: matrix
//        anchors.centerIn: parent
        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.left: parent.left
//        anchors.right: parent.right
//        anchors.verticalCenter: parent.verticalCenter
        property int sizeOfCell: Math.min(mainItem.width, mainItem.height
                                          -bottom_row.height - listview.height)/9
        width: matrix.sizeOfCell * 9
        height: width
        interactive: false
        cellWidth: matrix.sizeOfCell
        cellHeight: cellWidth
        property int selected
        function setValueToSelected(value)
        {
            if(selected !== undefined)
                model.set(selected, {"number": value})
        }

        model: ListModel{
            id: myModel
            
            function prepareModel(){
                if(myModel.count == 0)
                    for(var i = 0; i < 81; i++)
                        myModel.append({"number": "", "blocked": false, 
                                           "marked": false, "dimed": false,
                                           "isGuess": false,
                                           "defaultOddEven": "default"})
            }
            
            Component.onCompleted: 
            {
                prepareModel();
            }
        }
        
        function makeItteration()
        {
            sudoku.makeItteration()
            setCells(sudoku.getString())
        }
        
        function clear(){
            for(var i=0; i<81; i++)
            {
                model.set(i, {"number": "", "blocked": false, 
                              "marked": false, "dimed": false,
                              "isGuess": false,
                              "defaultOddEven": "default"})
            }
        }
        
        function setCells(cellsState){
            myModel.prepareModel();
            for(var i = 0; i < cellsState.length; i++)
            {
                if(cellsState[i] !== '.')
                {
                    model.set(i, {"number": cellsState[i]})
                    model.set(i, {"blocked": true})
                }
                else
                {
                    model.set(i, {"number": ''})
                    model.set(i, {"blocked": false})
                }
            }
        }
        
        function clearEmptyCells(indexesToClear)
        {
            for(var i = 0; i < indexesToClear.length; i++)
            {
                model.set(indexesToClear[i], {"number": ''})
                model.set(indexesToClear[i], {"blocked": false})
                model.set(indexesToClear[i], {"isGuess": false})
            }
        }
        
        function insertInModel(x, y, key, value)
        {
            model.get(x+y*9)[key] = value
        }
        
        function setState(){
            var str = '';
            var default_odd_even = []
            for(var i = 0; i < model.count; i++)
            {
                var element = model.get(i)
                str += element['number'].length > 0 ? element['number'] : '.';
                default_odd_even.push(element['defaultOddEven'] === "default" ?
                                          0 : (element['defaultOddEven'] === "odd" ? 1 : 2))
            }
            sudoku.clear();
            sudoku.setDefaultOddEven(default_odd_even);
            var successful = sudoku.setInitialString(str);
            return successful;
        }
        
        delegate: Rectangle{
            
            id: cell
            //            color: dimed ? "#E0E0E0" : "white"
            color: defaultOddEven == "default" ? "white" : defaultOddEven == "odd" ? "#E0E0E0" : "#f2c2ff"
            width: matrix.sizeOfCell
            height: width
            border.color: isSelected ? "black" : "#9E9E9E"
            layer.enabled: true
            property bool isSelected: false
            onIsSelectedChanged: {
                if(isSelected)
                    matrix.selected = index;
            }

            property bool isGlowing: isSelected
            property bool isGuess: false
            onIsGlowingChanged: {
                console.log("Glowing", isGlowing)
                if(isGlowing)
                    z+=2;
                else
                    z-=2;
            }
            
            
            Behavior on color { PropertyAnimation {
                    duration: 500
                    easing.type: color !== 'white' ? Easing.OutElastic : Easing.Linear
                }}
            
//            layer.effect: Glow {
//                spread: 1
//                samples: 15
//                color: isSelected ? (/*isGuess ? "#9c27b0" : */ "#607D8B") : marked ? "#FFCC80" :  "transparent"
//                transparentBorder: true
//                Behavior on color { PropertyAnimation {
//                        duration: 250
//                        easing.type: Easing.Linear
//                    }}
//            }
            
            property bool isWrong: false
            
            Text{   
                id: textElem
                anchors.centerIn: parent
                text: number
                font.pointSize: 26 * matrix.sizeOfCell/50
                horizontalAlignment: Text.AlignHCenter
                onTextChanged: {
                    if(!text)
                        parent.isWrong = false;
                }
                
                color: parent.isWrong ? "#B00020" : parent.isGuess ? 
                                            "#9c27b0" : "black"
            }
            function userMadeStep()
            {
                var response = sudoku.userStep(index, number);
                if(response.cellToBeFree)
                    matrix.model.set(response.cellToBeFree, {"blocked": false})
                if(response.isFallback)
                {
                    console.log("fallback")
                    matrix.clearEmptyCells(response.cellsIndexesToClear)
                }
                if(response.message === "ok"){
                    isGuess = response.isGuess;
                    isWrong = false;
                    marked = false;
                    blocked = true;
                    if(anim.isSeen)
                        hider_anim.start();
                }
                else{
                    matrix.insertInModel(response.x, response.y, "marked", true);
                    isWrong = true;
                }
            }
            
            MouseArea{
                id: m_area
                anchors.fill: parent
                onClicked: {
                    forceActiveFocus()
                }
                
                onDoubleClicked: {
                    if(defaultOddEven === "even")
                        defaultOddEven = "default";
                    else
                        defaultOddEven = "even"
                    
                }
                
                onPressAndHold: {
                    if(defaultOddEven === "odd")
                        defaultOddEven = "default";
                    else
                        defaultOddEven = "odd"
                }
                
                onFocusChanged:
                {
                    console.log("Focus", focus)
                    if(focus)
                        parent.isSelected = true
                    else
                    {
                        parent.isSelected = false
                    }
                }
                Keys.onPressed: {
                    if(!blocked){
                        if(event.text > 0 && event.text <= 9)
                            number = event.text;
                        else if(event.key === Qt.Key_Backspace || 
                                event.key === Qt.Key_Space ||
                                event.key === Qt.Key_0)
                            number = ''
                    }
                }
            }
        }
        
        property int widthOfBorder: matrix.sizeOfCell/18
        Rectangle{
            x: 0
            y: matrix.height/3
            height: matrix.widthOfBorder
            width: matrix.width
            color: "black"
        }
        Rectangle{
            x: 0
            y: matrix.height*2/3
            width: matrix.width
            height: matrix.widthOfBorder
            color: "black"
        }
        Rectangle{
            x: matrix.width/3
            y: 0
            color: "black"
            height: matrix.height
            width: matrix.widthOfBorder
        }
        Rectangle{
            x: matrix.width*2/3
            y: 0
            color: "black"
            height: matrix.height
            width: matrix.widthOfBorder
        }
    }
    
    ListView{
        id: listview
//        anchors.top: matrix.bottom
//        anchors.topMargin: height*0.1
        anchors.left: parent.left
        anchors.right: parent.right
        height: delegateSize
        orientation: ListView.Horizontal 
        property real delegateSize: listview.width/10
        model: 10
        delegate: Rectangle{
            width: listview.delegateSize
            height: width
            Text {
                text: index
                anchors.centerIn: parent
                width: parent.width
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    matrix.setValueToSelected(index !== 0 ? index.toString() : "")
                }
            }
        }
    }
    
    Row{
//        anchors.top: listview.bottom
        id: bottom_row
        anchors.horizontalCenter: matrix.horizontalCenter
//        anchors.topMargin: height*0.1
        spacing: anchors.topMargin
        Button{
            text: confirmStage ? "Sure?" : "Clear" 
            property bool confirmStage: false
            Timer{
                id: timer
                interval: 3000
                onTriggered: {
                    console.log("triggered")
                    parent.confirmStage = false}
            }
            onConfirmStageChanged: {
                if(confirmStage)
                    timer.start()
            }
            
            onClicked: {
                if(confirmStage)
                {
                    matrix.clear()
                    timer.stop()
                    confirmStage = false
                }
                else
                    confirmStage = true
                
            }
        }
        Button{
            text: "Solve"
            onClicked: {
                var successful = matrix.setState();
                if(successful)
                    successful = sudoku.solveAndShow();
                if(!successful)
                    popup.open()
            }
        }
    }
    }
}
