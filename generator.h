#pragma once

#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <QString>

#define UNASSIGNED 0

using namespace std;

class Generator {
private:
  int grid[9][9];
  int solnGrid[9][9];
  int guessNum[9];
  int gridPos[81];
  int difficultyLevel;

public:
  Generator ();
  void createSeed();
  QString getString();
  bool solveGrid();
  void countSoln(int &number);
  void genPuzzle();
  void printSVG(string);
  void calculateDifficulty();
  int  branchDifficultyScore();
};

