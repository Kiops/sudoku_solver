#include "sudoku.h"
#include <generator.h>
#include <QtDebug>
#include <time.h>

Prediction::Prediction()
{
    //this should be blank
}

Prediction::Prediction(int x, int y, int value, QVector <QVector<int>> matrix, Possible possible)
{
    this->x = x;
    this->y = y;
    this->value = value;
    this->backupMatrix = matrix;
    this->backupPossible = possible;
}

QVariantMap Sudoku::userStep(int index, int digit)
{
    int y = index / 9;
    int x = index % 9;
    QVariantMap result;
    QList<QVariant> cellsIndexesToClear;
    
    //skip fallbacks
    auto num_of_fallbacks = 0;
    while(loggedSteps[current_step_index].type == FALLBACK
          && current_step_index < loggedSteps.size())
    {
        num_of_fallbacks++;
    }
    if(loggedSteps[current_step_index].type == FALLBACK)
    {
        result.insert("isFallback", loggedSteps[current_step_index].type == FALLBACK);
        result.insert("cellsState", getString());

        for(auto step_index = current_step_index, fallbacks = num_of_fallbacks; fallbacks>0 && step_index >= 0; step_index--){
            if(loggedSteps[step_index].type == FALLBACK)
            {
                fallbacks++;
                continue;
            }
            possible.notify(loggedSteps[step_index].x, loggedSteps[step_index].y, loggedSteps[step_index].digit, true);
            cellsIndexesToClear.append(loggedSteps[step_index].x + loggedSteps[step_index].y*9);
            if(loggedSteps[step_index].type == GUESS)
                fallbacks--;
        }
        current_step_index+=num_of_fallbacks;
    }

    result.insert("cellsIndexesToClear", cellsIndexesToClear);
    result.insert("cellToBeFree", 
                  loggedSteps[current_step_index+1].x + 
            loggedSteps[current_step_index+1].y*9);
    
    auto& required_step = loggedSteps[current_step_index];
    result.insert("x", required_step.x);
    result.insert("y", required_step.y);
    result.insert("digit", required_step.digit);
    if(required_step.x == x && 
            required_step.y == y && 
            required_step.digit == digit)
    {
        current_step_index++;
        result.insert("message", "ok");
        result.insert("isGuess", required_step.type == GUESS);
        this->setDigit(x, y, digit, NO_LOG);
    }
    else {
        result.insert("message", "wrong");
        result.insert("correct_digit", required_step.digit);
        result.insert("row", QVariant::fromValue(possible.getPossibleInRow(required_step.y)));
        result.insert("col", QVariant::fromValue(possible.getPossibleInColumn(required_step.x)));
        result.insert("rec", QVariant::fromValue(possible.getPossibleInRectangle(required_step.x, required_step.y)));
   }
    return result;
}

Step Sudoku::findGuessPoint()
{
    //find dot with minimal amount of possible digits
    QVector <int> minimalPossible;
    auto minimal = 10;
    auto xMin = 0;
    auto yMin = 0;
    
    QVector<int> possibleDigits;
    for(auto i = 0; i < 9; i++)
        for(auto j = 0; j < 9; j++)
        {
            if(matrix[i][j] != 0)
                continue;
            possibleDigits = getPossible(j,i);
            if(possibleDigits.size() > 0 && possibleDigits.size() < minimal)
            {
                minimal = possibleDigits.size();
                minimalPossible = possibleDigits;
                xMin = j;
                yMin = i;
            }
        }
    if(minimal == 10)
        return Step();
    return Step(xMin, yMin, minimalPossible.first());
}

void Sudoku::makeGuess()
{
    auto guess = findGuessPoint();
    if(!guess.isValid)
        return;
    saveGuess(guess.x, guess.y, guess.digit);
    //    qDebug() << "Guess" << guess.digit;
}

void Sudoku::saveGuess(int x, int y, int digit){
    guesses.push_back(Prediction(x, y, digit, matrix, possible));
    setDigit(x, y, digit, GUESS);
}

Prediction Sudoku::fallbackToNGuesses(int n)
{
    auto desired_index = guesses.length() - n;
    if(desired_index < 0)
        throw PossibleException("No solution for this sudoku");
    auto guess = guesses.at(desired_index);
    
    //fallback
    matrix = guess.backupMatrix;
    possible = guess.backupPossible;
    
    //clear n-last
    guesses.erase(guesses.begin()+desired_index, guesses.end());
    
    //    qDebug() << guesses.length();
    loggedSteps.push_back(LoggedRecord(guess.x, guess.y, guess.value, FALLBACK));
    return guess;
}

Prediction Sudoku::fallbackToGuess(int x, int y)
{
    for(int i = 1; i < guesses.size(); i++)
    {
        auto guess_it = guesses.end()-1;
        if(guess_it->x == x && guess_it->y == y)
            return fallbackToNGuesses(i);
    }
    throw QString("Requested fallback to non listed guess");
}

void Sudoku::correctGuess()
{
    auto guess = fallbackToNGuesses(1);
    QVector<int> possibleDigits = getPossible(guess.x, guess.y);
    int indexOfLastTry = possibleDigits.indexOf(guess.value);
    if(indexOfLastTry == possibleDigits.size()-1)
        correctGuess();
    else
        saveGuess(guess.x, guess.y, possibleDigits[indexOfLastTry+1]);
}

Sudoku::Sudoku(QObject *parent) :
    QObject(parent)
{
    matrix.fill(QVector<int>(9, 0), 9);
}

bool Sudoku::setInitialString(QString input_string)
{
    //    clear();
    initial_string = input_string;
    try {
        for(auto i = 0; i < 9; i++)
            for(auto j = 0; j < 9; j++)
            {
                if(initial_string[i*9+j] != '.')
                {
                    setDigit(j, i, initial_string[i*9+j].digitValue(), NO_LOG);
                }
            }
    } catch (PossibleException except) {
        return false;
    }
    return true;
}

void Sudoku::setDefaultOddEven(QList<int> default_odd_even)
{
    possible.setPariaty(default_odd_even);
}

bool Sudoku::solveAndShow()
{
    try {
        solve();
        emit updateVisualState();  
    } catch (...) {
        return false;
    }
    return true;

}

int Sudoku::matrixElement(int x, int y) const
{
    return matrix[y][x];
}

QString Sudoku::getString()
{
    QString result;
    for(int i = 0; i < matrix.size(); i++)
        for(int j = 0; j < matrix[i].size(); j++)
            matrix[i][j] > 0 ? result.push_back(QString::number(matrix[i][j])) : result.push_back('.');
    return result;
}

bool Sudoku::isSolved() const
{
    for(auto i = 0; i < matrix.size(); i++)
        for(auto j = 0; j < matrix[i].size(); j++)
        {
            if(matrix[j][i] == 0)
                return false;
            if(getPossible(j,i).size() != 0)
                return false;
        }
    return true;
}

QVector<int> Sudoku::getPossible(int x, int y) const
{
    return possible.getPossible(x,y);
}

void Sudoku::solve()
{
    while (true)
    {
        bool changed_state = false;
        for(auto i = 0; i < 9; i++)
            for(auto j = 0; j < 9; j++)
            {
                if(matrix[i][j] == 0)
                {
                    QVector<int> pos = getPossible(j,i);
                    if(pos.size() == 1)
                    {
                        setDigit(j, i, pos[0]);
                        changed_state = true;
                    }
                    else {
                        if(pos.isEmpty())
                        {
                            changed_state = true;
                            correctGuess();
                        }
                    }
                }
            }
        if(isSolved())
            return;
        if(!changed_state)
        {  
            makeGuess();
        }
    }
}

QString Sudoku::makeItteration()
{
    bool changed_state = false;
    for(auto i = 0; i < 9; i++)
        for(auto j = 0; j < 9; j++)
        {
            if(matrix[i][j] == 0)
            {
                QVector<int> pos = getPossible(j,i);
                if(pos.size() == 1)
                {
                    setDigit(j, i, pos[0]);
                    changed_state = true;
                }
                else {
                    if(pos.isEmpty())
                    {
                        changed_state = true;
                        correctGuess();
                    }
                }
            }
        }
    if(isSolved())
        return getString();
    if(!changed_state)
    {
        makeGuess();
    }
    return getString();
}

void Sudoku::clear(bool clear_logged_steps)
{
    possible = Possible();
    current_step_index = 0;
    guesses.clear();
    matrix.clear();
    matrix.fill(QVector<int>(9, 0), 9);
    initial_string.clear();
    //    QString output;
    //    for(auto it = loggedSteps.begin(); it != loggedSteps.end(); it++)
    //    {
    //        if(it->type == SIMPLE)
    //            output += "S";
    //        else if(it->type == GUESS)
    //            output += "G";
    //        else 
    //            output += "F";
    //        output += "{";
    //        output += QString::number(it->x) + ":" + QString::number(it->y) 
    //                + ":" + QString::number(it->digit) + "} ";
    //    }
    if(clear_logged_steps)
        loggedSteps.clear();
}

void Sudoku::setDigit(int x, int y, int digit, LoggedType stepType)
{
    matrix[y][x] = digit;
    possible.notify(x, y, digit);
    if(stepType != NO_LOG)
        loggedSteps.push_back(LoggedRecord(x, y, digit, stepType));
}

void Sudoku::prepare(){
    //    srand (time(NULL));
    Generator puzzle;
    puzzle.createSeed();
    puzzle.genPuzzle();
    puzzle.calculateDifficulty();
    setInitialString(puzzle.getString());
    solve();
    cleanUpForUserPlay();
    emit updateVisualState();
}

void Sudoku::cleanUpForUserPlay()
{
    auto initial_string_backup = initial_string;
    clear(false);
    setInitialString(initial_string_backup);
}
